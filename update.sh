#! /bin/bash

git submodule init
git submodule update
git submodule foreach git pull origin master

pushd bundle/tern_for_vim
npm install
popd

pushd bundle/vimproc 
make
popd

