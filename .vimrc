filetype off
filetype plugin indent off
call pathogen#infect()
set rtp+=$GOROOT/misc/vim
filetype plugin indent on
syntax on
""""""""""""""""""""""""""""""""""""
" Set shell config on windows
""""""""""""""""""""""""""""""""""""

if executable('sh.exe')
  set shell=sh.exe\ -login
  set shellcmdflag=-c
  set shellquote="
  set shellslash
  " If shell is available, so is grep
  set grepprg=grep\ -nH
endif

""""""""""""""""""""""""""""""""""""
" General plugin configurations
""""""""""""""""""""""""""""""""""""

"""" SuperTab """"

let g:SuperTabDefaultCompletionType = "context"
let g:SuperTabContextDefaultCompletionType = "<C-x><C-o>"
let g:SuperTabContextTextOmniPrecedence = ['&omnifunc', '&completefunc']

"""" Syntastic """"
" Disable error display on cursor move
let g:syntastic_echo_current_error=0
let g:syntastic_php_checkers=['php']
let g:syntastic_phpcs_disable=1
let g:syntastic_php_phpmd_post_args="text codesize,unusedcode,design" 
" Always populate error list
let g:syntastic_always_populate_loc_list=1

"""" PHP Documentor """"
let g:pdv_template_dir = $HOME . "/vimfiles/bundle/pdv/templates_snip"
autocmd FileType php nnoremap <buffer> <F12> :call pdv#DocumentWithSnip()<CR>

" Enable code folding for Vimwiki
let g:vimwiki_folding = 1

"""" Javascript libs """"
let g:used_javascript_libs = 'underscore,backbone,angularjs,jquery'

" Quickfix, sometimes you just want a tab
inoremap <leader><TAB> <TAB>


"""" NERDTree """"
" Let NERDTree decide cwd (top of current tree)
let g:NERDTreeChDirMode=2
" Close NERDTree after opening files
let g:NERDTreeQuitOnOpen=1
"Map F8 to NERDTreeToggle 
map <F8> :NERDTreeToggle<cr>

"""" Tagbar """"
"Map F7 to tagbar
nmap <F7> :TagbarToggle<cr>
let g:tagbar_autoclose=1
let g:tagbar_left=1

"""" CtrlP """"
let g:ctrlp_custom_ignore = 'node_modules'

""" CoffeScript """
let g:coffee_lint_options = {'max_line_length' : 0}


""""""""""""""""""""""""""""""""""""
" General setup
""""""""""""""""""""""""""""""""""""

let mapleader=","

" Some PHP interpereters output errors in an unexpected format
set efm+=",%f|%l %trror| %m"

"only javascript indent one level for multiple brackets on same line (jQuery)
let g:SimpleJsIndenter_BriefMode = 1

"Syntax highlight sql queries
let g:php_sql_query=1
"Syntax highlight html in strings
let g:php_htmlInStrings=1

"Auto generate folds by indent
set foldmethod=marker
set foldmarker={,}
set foldminlines=3
set foldlevelstart=99
set foldlevel=99

syntax on
filetype plugin indent on

"Fix indents
set tabstop=2
set softtabstop=2
set shiftwidth=2
"Do not expand tabs
set noexpandtab

set relativenumber

"Allow backspace to do its thing
set backspace=indent,eol,start


"Take a guess
set enc=utf-8
set fileformats=unix,dos

"Windows specific configuration
if has('win32') || has('win64')
    "Set font
    set gfn=DejaVu_Sans_Mono_for_Powerline:h10
    let g:ruby_path="c:\Ruby193\bin"
endif

"Change default folder for ~ and .swp files
if( expand($TEMP) == "" )
    "Default for linux
    let s:tempFolder = "/tmp"
else
    "Default for windows
    let s:tempFolder = expand($TEMP)
endif
let &directory=s:tempFolder
let &backupdir=s:tempFolder

"Ignore case on searches, unless search contains uppercase
set ignorecase smartcase

set completeopt=menuone,menu,longest,preview

"Highlight searchresults
set hlsearch
"Display suggestions for commands
set wildmenu wildmode=list:full
set incsearch

"Hide files from plugins
set wildignore=*/.git/*

let g:indent_guides_enable_on_vim_startup = 1

"Compile .as files using enter
autocmd BufNewFile,BufRead *.as nnoremap <buffer> <CR> !mxmlc %<CR>
autocmd BufNewFile,BufRead *.as set filetype=actionscript


"Set colorscheme
if has("gui_running")
set background=dark
colorscheme solarized
endif

"return the syntax highlight group under the cursor ''
function! StatuslineCurrentHighlight()
    let name = synIDattr(synID(line('.'),col('.'),1),'name')
    if name == ''
        return ''
    else
        return '[' . name . ']'
    endif
endfunction

"recalculate the tab warning flag when loading and after writing
autocmd BufReadPost,bufwritepost * unlet! b:statusline_tab_warning

"return '[&et]' if &et is set wrong
"return '[mixed-indenting]' if spaces and tabs are used to indent
"return an empty string if everything is fine
function! StatuslineTabWarning()
    if !exists("b:statusline_tab_warning")
        let b:statusline_tab_warning = ''

        if !&modifiable
            return b:statusline_tab_warning
        endif

        let tabs = search('^\t', 'nw') != 0

        "find spaces that arent used as alignment in the first indent column
        let spaces = search('^ \{' . &ts . ',}[^\t]', 'nw') != 0

        if tabs && spaces
            let b:statusline_tab_warning = '[mixed-indenting]'
        elseif (spaces && !&et) || (tabs && &et)
            let b:statusline_tab_warning = ' [&et]'
        endif
    endif
    return b:statusline_tab_warning
endfunction

"statusline setup
set statusline=%f "tail of the filename
set statusline+=\ 

"display a warning if fileformat isnt unix
set statusline+=%#warningmsg#
set statusline+=%{&ff!='unix'?'['.&ff.']':''}
set statusline+=%*

"display a warning if file encoding isnt utf-8
set statusline+=%{(&fenc!='')?'['.&fenc.']':''}

set statusline+=%h "help file flag
set statusline+=%y "filetype
set statusline+=%r "read only flag
set statusline+=%m "modified flag

"display a warning if &et is wrong, or we have mixed-indenting
set statusline+=%#error#
set statusline+=%{StatuslineTabWarning()}
set statusline+=%*

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

set statusline+=%= "left/right separator

function! SlSpace()
    if exists("*GetSpaceMovement")
        return "[" . GetSpaceMovement() . "]"
    else
        return ""
    endif
endfunc
set statusline+=%{SlSpace()}

set statusline+=%{StatuslineCurrentHighlight()}\ \ "current highlight
set statusline+=%c, "cursor column
set statusline+=%l/%L "cursor line/total lines
set statusline+=\ %P "percent through file
set laststatus=2

"Remap gd to go to definition
nnoremap gd <C-]>
inoremap kj <ESC>

"Insert current time with F5
inoremap <F5> <C-R>=strftime("%H:%M:%S")<CR>
"Insert current date with F4
inoremap <F4> <C-R>=strftime("%d-%m-%Y")<CR>

nnoremap - :Switch<cr>
let g:detectindent_prefered_indent = 2
let g:detectindent_preferred_expandtab = 1
autocmd BufReadPost * :DetectIndent

"Treat blade files as html files
au BufNewFile,BufRead *.blade.php set filetype=html

"Set visual bell
set vb

au BufCreate ~/vimwiki/index.wiki :call vimproc#system("git --git-dir ~/vimwiki/.git --work-tree ~/vimwiki/ pull origin master")
au BufWritePost ~/vimwiki/* :call vimproc#system("git --git-dir ~/vimwiki/.git --work-tree ~/vimwiki/ add .")
au BufWritePost ~/vimwiki/* :call vimproc#system("git --git-dir ~/vimwiki/.git --work-tree ~/vimwiki/ commit -a -m ' - '")
au BufUnload ~/vimwiki/* :call vimproc#system("git --git-dir ~/vimwiki/.git --work-tree ~/vimwiki/ push origin master")
au BufWritePre *.go Fmt
let g:gofmt_command = "goimports"

